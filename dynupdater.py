#!/usr/bin/env python3
# Joker.com dynupdater
# 2012 adrian@javaguru.org

# Sample config file /etc/dynupdater.conf  Use chmod 600
# DYNA / domain properties
# userName = 76c2ccef53d1
# passWord = ca205ee41524
# hostName = xxx.javaguru.org
from doctest import FAIL_FAST

from http.client import HTTPConnection
import sys
import os
import subprocess
import syslog
import platform
from plistlib import load, dump, FMT_BINARY
import socket


configFile = None
if (platform.system() == "Linux") or (platform.system() == "Darwin"):
    print("Found UNIX OS")
    configFile = "/etc/dynupdater.conf"
elif platform.system() == "Windows":
    print("Found Windows OS")
    configFile = "dynupdater.conf"

if os.path.exists(configFile):
    print("Found configuration: " + configFile)
    file = open(configFile, "r")
    conf = {}
    for line in file:
        if not line.startswith('#'):
            # result of partition is a tuple
            param = line.partition("=")
            conf[param[0].strip()] = param[2].strip()

    userName = conf.get("userName")
    print(userName)
    passWord = conf.get("passWord")
    print(passWord)
    hostName = conf.get("hostName")
    print(hostName)
    if hostName is None:
        print("Configuration file error. Edit /etc/dynupdater.conf manually")
        exit(1)
else:
    with open(configFile, 'w') as cfg:
        print("Could not find config. Creating - do not interrupt, or the file will be incomplete")
        cfg.write("# Created by DynUpdater script at runtime\n")
        cfg.write("userName=" + input("Enter Joker username:") + "\n")
        cfg.write("passWord=" + input("Enter Joker passWord:") + "\n")
        cfg.write("hostName=" + input("Enter Joker hostName:") + "\n")
        print("Configuration file created. Rerun script")
        exit(0)


# server, service
jokerIPService = "http://svc.joker.com/nic/update?"
server = "svc.joker.com"
service = "/nic/update?"
paramString = "username=" + userName + "&password=" + passWord + "&hostname=" + hostName

#params = urlencode({'username': userName, 'password': passWord, 'hostname': hostName})
headers = {"Accept": "text/plain", "Cache-Control": "no-Cache"}
conn = HTTPConnection(server)
string = ""
try:
    conn.set_debuglevel(0)
    conn.request("GET", service+paramString)
    rsp = conn.getresponse().read().decode()
    print("Joker DNS service updater: ", rsp)
    ping = subprocess.Popen(
        ["ping", "-c", "1", hostName],
        stdout = subprocess.PIPE,
        stderr = subprocess.PIPE
    )
    out, error = ping.communicate()
    #print("Ping: " , out,error)
    string = out.decode("utf-8")
    #print(hostName + " updated to " + string[string.find('(') +1: string.find(')')] )
    if (platform.system() == "Linux") or (platform.system() == "Darwin"):
        syslog.syslog(syslog.LOG_INFO, hostName + ": " + rsp)
except socket.gaierror as err:
    print("Connection error : ", err)

